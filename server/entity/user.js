/**
 * @orm:attribute {username: STRING|UNIQUE}
 * @orm:attribute {password: STRING}
 * @orm:attribute {tag: STRING|UNIQUE}
 * @orm:attribute {credit: FLOAT}
 * @orm:timestamp false
 */
class User {
    constructor(id, username, password, tag, credit){
        this.id = id;
        this.username = username;
        this.password = password;
        this.tag = tag;
        this.credit = credit;
    }

    /**
     * @param {Object} data
     * @returns {User}
     */
    static initFromDb(data){
        return new User(data.id, data.username, data.password, data.tag, data.credit);
    }

    getData(){
        return [this.username, this.password, this.tag, this.credit];
    }
}

exports.User = User;