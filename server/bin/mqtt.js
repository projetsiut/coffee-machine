const mqtt = require('mqtt');
const model = require('../models/user');

const mqtt_color = "\x1b[34m";
const reset = "\x1b[0m";

class MqttHandler{

    constructor(db){
        this.usermodel = new model.UserModel(db);
        this.client = mqtt.connect('mqtt://guillaume-marmorat.com');

        this.client.on('connect', () => {
            this.client.subscribe('coffee');
            console.log(mqtt_color, '\r[mqtt] listening coffee topic.', reset);
        });

        this.client.on('message', (topic, message) => {
            if (topic === 'coffee') {
                if (/^update\/credit\/[0-9*]\/[0-9*]/g.test(message.toString())){
                    let data = message.toString().split('/');
                    this.updateUserCredit(data[2], data[3]);
                } else if (/^get\/credit\/[0-9*]/g.test(message.toString())){
                    let data = message.toString().split('/');
                    this.getUserCredit(data[2]);
                } else if (/^set\/credit\/[0-9*]\/?\d+(\.\d+)?/g.test(message.toString())){
                    let data = message.toString().split('/');
                    this.setUserCredit(data[2], data[3]);
                }
            }

            // console.log(message.toString());
        });
    }

    terminate(){
        this.client.end();
    }

    getUserCredit(user_tag){
        this.usermodel.retreive({tag: user_tag}).then((user) => {
            this.client.publish('coffee', 'res/'+user.tag+'/'+user.credit);
        }).catch(() => {
            this.client.publish('coffee', 'err/'+user_tag+'/not_found')
        });
    }

    setUserCredit(user_tag, credit) {
        this.usermodel.retreive({tag: user_tag}).then((user) => {
            user.credit = credit;
            this.usermodel.update(user).then(() => {
                this.client.publish('coffee', 'res/success');
            });
        }).catch(() => {
            this.client.publish('coffee', 'res/not_found')
        })
    }

    updateUserCredit(user_tag, credit_to_sub) {
        this.usermodel.retreive({tag: user_tag}).then((user) => {
            user.credit -= credit_to_sub;
            this.usermodel.update(user).then(() => {
                this.client.publish('coffee', 'res/success');
            });
        }).catch(() => {
            this.client.publish('coffee', 'res/not_found')

        })
    }
}

module.exports = MqttHandler;