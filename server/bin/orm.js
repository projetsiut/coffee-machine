const path = require('path');
const fs = require('fs');
const annotations = require('annotations');

const orm_color = "\x1b[33m";
const reset = "\x1b[0m";

/**
 * Db type declaration
 * @type {{FLOAT: string, UNIQUE: string, STRING: string, INT: string}}
 */
const Types = {
    STRING: 'varchar(128)',
    FLOAT: 'float',
    INT: 'integer',
    UNIQUE: 'unique',
};

/**
 * Kind of ORM to map entity with db tables, use sync to create tables wich not already exist.
 * Use annotations to define tables attributes.
 */
class ORM {
    constructor(db, directoryPath = null){
        this.db = db;
        this._dirPath = (directoryPath !== null) ? directoryPath : path.join(__dirname, '../entity');
        this.annotations_data = [];
    }

    /**
     * Call to sync entity with db
     */
    sync(){
        this._initAnnotations().then((result) => {
            this.annotations_data = result;
            console.log(orm_color, '\r[orm] annotations found : ', this.annotations_data, reset);
        }).then(() => {
            for (let table of this.annotations_data) {
                this._syncTable(table);
            }
            console.log(orm_color, '\r[orm] syncing db with entity.', reset);
        });
    }

    _initAnnotations() {
        return new Promise((resolve, reject) => {
            //passing directoryPath and callback function
            fs.readdir(this._dirPath, (err, files) => {
                let result = [];
                //handling error
                if (err) {
                    reject('Unable to scan directory: ' + err);
                }
                //listing all files using forEach
                files.forEach((file) => {
                    result.push(annotations.getSync(this._dirPath + '/' + file));
                });
                resolve(result);
            });
        });
    }

    _syncTable(tableData) {
        let tableName = Object.keys(tableData)[0];
        this.db.query('SHOW TABLES LIKE \''+tableName+'\'').then((showResult) => {
            if (showResult.length > 0){
                console.log(orm_color, '\r[orm] table :', tableName, 'already exist, nothing to do.', reset)
            } else {
                let sql = `CREATE TABLE IF NOT EXISTS ${tableName}(id int auto_increment primary key,`;
                let field_list = [];
                let unique_col_flag = false;

                for (let column of tableData[tableName]['orm:attribute']) {
                    if (column === '{') { // case only one attribute
                        sql += this._handleRow(tableData[tableName]['orm:attribute']) + ')';
                        unique_col_flag = true;
                        break;
                    } else {
                        field_list.push(this._handleRow(column));
                    }
                }

                if (!unique_col_flag)
                    sql += field_list.join(',') + ')';

                sql += ';';
                console.log(orm_color, '\r[orm] found ', tableName, 'which not exist : ', sql, reset);

                this.db.query(sql).then((insertResult, err) => {
                    if (err) throw new Error('Unable to create table.');
                    console.log(orm_color, '\r[orm] created table', tableName, reset);
                });
            }
        });
    }

    _handleRow(string){
        let colName;
        let colType;
        let constraints = '';
        let splitted;

        string = string.replace(/[{}]/g, '');
        splitted = string.split(':');
        colName = splitted[0];

        if (string.includes('|')) {
            for (let elm of splitted[1].split('|')){
                if (colType === undefined)
                    colType = Types[elm.replace(' ', '')];
                else
                    constraints += Types[elm];
            }
        } else {
            colType = Types[splitted[1].replace(' ', '')];
        }

        if (constraints !== '')
            return colName+' '+colType+' '+constraints;
        return colName+' '+colType;
    }
}

module.exports = ORM;



