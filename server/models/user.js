const mysql = require('promise-mysql');
const user = require('../entity/user');

class UserModel {
    constructor(db){
        this.db = db;
    }

    /**
     * Create new user in db
     * @param {User} user
     */
    create(user){
        return new Promise((resolve, reject) => {
            let sql;

            sql = "INSERT INTO User VALUES (DEFAULT, ?, ?, ?, ?)";
            sql = mysql.format(sql, user.getData());

            this.db.query(sql).then((result, err) => {
                if (err) resolve(false);
                resolve(true);
            })
        });
    }

    /**
     * Retreive user
     * @param {Object} data, with {tag: <> or username: <>} defined
     */
    retreive(data = null){
        return new Promise((resolve, reject) => {
            let outData = null;
            let sql;
            let id_flag = false;

            if (data !== null){

                if (data.hasOwnProperty('id')) {
                    sql = `SELECT * FROM User WHERE id=${this.db.escape(data.id)}`;
                    id_flag = true;
                } else if (data.hasOwnProperty('tag')) {
                    sql = `SELECT * FROM User WHERE tag=${this.db.escape(data.tag)}`;
                    id_flag = true;
                } else {
                    sql = "SELECT * FROM User WHERE ? = ?";
                    let inserts = [];
                    for (let prop of Object.keys(data)) {
                        inserts.push(prop);
                        inserts.push(data[prop]);
                    }
                    sql = mysql.format(sql, inserts);
                    id_flag = true;
                }
            }
            else
                sql = "SELECT * FROM User";

            // execute query
            this.db.query(sql, (err, result) => {
                if (err){
                    reject(err);
                } else if (result.length === 0) {
                    reject('not found');
                } else {
                    if (id_flag){
                        outData = user.User.initFromDb(result[0]);
                    } else {
                        outData = [];
                        for (let elm of result) {
                            outData.push(user.User.initFromDb(elm))
                        }
                    }
                    resolve(outData);
                }
            });
        });
    }

    update(user){
        return new Promise((resolve, reject) => {
            let sql = `UPDATE User SET credit=${user.credit} WHERE id=${user.id}`;

            this.db.query(sql).then((result, err) => {
                if (err) resolve(false);
                resolve(true);
            })
        });
    }
}

module.exports.UserModel = UserModel;
