const express = require('express');
const router = express.Router();
const uuid = require('uuid/v1');
const model = require('../models/user');
const user = require('../entity/user');

router.post('/user/create', function (req, res) {
    let newUser = new user.User(0, req.body.username, req.body.password, uuid(), 0.0);
    let usermodel = new model.UserModel(global.db);

    usermodel.create(newUser).then((result) => {
        if (result) {
            res.redirect('/');
        } else {
            res.render('user/create', {
                title: 'Create a user',
                error: 'Failed to create user.'
            })
        }
    }).catch((err) => {
        res.status(503);
        res.send(err);
    });
});

router.post('/user/update', function (req, res) {
    let usermodel = new model.UserModel(global.db);

    usermodel.retreive({id: req.body.id}).then((user) => {
        user.credit = req.body.credit;

        usermodel.update(user).then((result) => {
            if (result) {
                res.redirect('/');
            } else {
                res.render('user/credit', {
                    title: 'Add credit',
                    error: 'Failed to create user.'
                })
            }
        });
    });
});

router.get('/user/:id', function(req, res) {
    let usermodel = new model.UserModel(global.db);
    usermodel.retreive({id: req.params.id}).then((user) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(user));
    }).catch((err) => {
        if (err === 'User not found')
            res.status(404);
        else
            res.status(503);

        res.send(err);
    });
});

module.exports = router;