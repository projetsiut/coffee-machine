var express = require('express');
var router = express.Router();
var usermodel = require('../models/user');

/* GET home page. */
router.get('/', function(req, res, next) {
    let model = new usermodel.UserModel(db);
    model.retreive().then((users) => {
        res.render('index', {
            title: 'Users list',
            users: users
        });
    }).catch((err) => {
        res.render('index', {
            title: 'Users list',
            users: []
        });
    });
});

module.exports = router;
