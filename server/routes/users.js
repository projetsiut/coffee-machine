const express = require('express');
const router = express.Router();
const model = require('../models/user');

/* GET users listing. */
router.get('/create', function(req, res, next) {
  res.render('user/create', {
    title: 'Create a user'
  });
});

router.get('/credit/:id', function (req, res) {

    let usermodel = new model.UserModel(global.db);

    usermodel.retreive({id: req.params.id}).then((user) => {
        res.render('user/credit', {
            title: 'Add credit',
            user: user
        });
    }).catch((err) => {
        res.status(503);
        res.send(err);
    });
});

module.exports = router;
